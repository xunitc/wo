﻿# Host: localhost  (Version 5.5.5-10.3.10-MariaDB)
# Date: 2019-04-10 16:21:28
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "project"
#

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL DEFAULT -1 COMMENT 'id of user who post this project',
  `worker_id` int(11) NOT NULL DEFAULT -1 COMMENT 'id of user who service this project',
  `state` int(11) NOT NULL DEFAULT -1 COMMENT 'State',
  `project_name` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Project Name',
  `project_text` varchar(5000) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Project Text',
  `post_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Post time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Project';

#
# Data for table "project"
#

INSERT INTO `project` VALUES (1,1,4,0,'test','test','2019-04-10 10:25:28'),(2,4,-1,-1,'maming test','maming test','2019-04-10 14:01:50'),(3,5,5,0,'c','c','2019-04-10 16:18:05');

#
# Structure for table "sec_role"
#

DROP TABLE IF EXISTS `sec_role`;
CREATE TABLE `sec_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Roles';

#
# Data for table "sec_role"
#

INSERT INTO `sec_role` VALUES (1,'admin'),(2,'engineer'),(3,'normal');

#
# Structure for table "sec_user"
#

DROP TABLE IF EXISTS `sec_user`;
CREATE TABLE `sec_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Username',
  `password` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Password',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_name` (`user_name`) COMMENT 'username'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users';

#
# Data for table "sec_user"
#

INSERT INTO `sec_user` VALUES (1,'admin','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),(4,'maming','5e6a408a964a01e2859dfe34d9fc88964bd3f494ab63f08e9f9bc9d767c2f8f2'),(5,'engineer1','b4b73d4bab61d1ffd4d3d2c38245f66d109dfc46920b7b047c0a0f448ac92d96');

#
# Structure for table "sec_user_role"
#

DROP TABLE IF EXISTS `sec_user_role`;
CREATE TABLE `sec_user_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'User_Id',
  `role_id` int(11) unsigned DEFAULT NULL COMMENT 'Role_Id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_role` (`user_id`,`role_id`),
  KEY `fk_ur_role_id` (`role_id`),
  CONSTRAINT `fk_ur_role_id` FOREIGN KEY (`role_id`) REFERENCES `sec_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ur_user_id` FOREIGN KEY (`user_id`) REFERENCES `sec_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User_Role_Map';

#
# Data for table "sec_user_role"
#

INSERT INTO `sec_user_role` VALUES (1,1,1),(2,1,2),(3,1,3),(4,4,2),(5,5,2);

#
# Structure for table "workorder"
#

DROP TABLE IF EXISTS `workorder`;
CREATE TABLE `workorder` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `work_text` varchar(5000) COLLATE utf8_bin DEFAULT '',
  `post_time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_project_id` (`project_id`),
  CONSTRAINT `fk_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Work Order';

#
# Data for table "workorder"
#

INSERT INTO `workorder` VALUES (1,1,4,'ddd','2019-04-10 16:07:53'),(2,3,5,'ww','2019-04-10 16:19:04'),(3,3,5,'cc','2019-04-10 16:19:12'),(4,3,5,'ddd','2019-04-10 16:20:43');

#
# View "view_project"
#

DROP VIEW IF EXISTS `view_project`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `view_project`
  AS
  SELECT
    `p`.`id`,
    `p`.`user_id`,
    `p`.`worker_id`,
    `su_poster`.`user_name` AS 'poster_name',
    `su_worker`.`user_name` AS 'worker_name',
    `p`.`state`,
    `p`.`project_name`,
    `p`.`project_text`,
    `p`.`post_time`
  FROM
    ((`project` p
      LEFT JOIN `sec_user` su_poster ON (`p`.`user_id` = `su_poster`.`id`))
      LEFT JOIN `sec_user` su_worker ON (`p`.`worker_id` = `su_worker`.`id`));

#
# View "view_workorder"
#

DROP VIEW IF EXISTS `view_workorder`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `view_workorder`
  AS
  SELECT
    `wo`.`id`,
    `wo`.`project_id`,
    `wo`.`work_text`,
    `wo`.`post_time`,
    `su`.`user_name` AS 'poster'
  FROM
    (`workorder` wo
      LEFT JOIN `sec_user` su ON (`wo`.`user_id` = `su`.`id`));
