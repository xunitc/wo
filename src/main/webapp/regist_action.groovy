import groovy.sql.*;
import groovy.json.JsonSlurper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Hex;
import java.security.MessageDigest;

final log = LoggerFactory.getLogger(this.getClass());

def p = request.reader.text;
def slurper = new JsonSlurper();
p = slurper.parseText(p);

response.setContentType("application/json; charset=utf-8");
try {
	def ctx = new javax.naming.InitialContext().lookup("java:/comp/env");
	def datasource = ctx.lookup("jdbc/mysql");
	def executer = Sql.newInstance(datasource);
	/* 注册用户，此用户带有 normal 角色，是普通用户 */
	/* 密码将编码为 sha256 */
	def hash = MessageDigest.getInstance("SHA-256").digest(p.password.getBytes("UTF-8"));
	def sha_password = Hex.encodeHexString(hash);
	String sql = "INSERT INTO sec_user(user_name, password) VALUES('${p.username}', '${sha_password}')";
	executer.withTransaction {
		def keys = executer.executeInsert(sql, p.username, sha_password);
		def user_id = keys[0][0];
		String sql_role = "SELECT id FROM sec_role WHERE role_name = ?";
		def role = executer.firstRow(sql_role, 'normal');
		String sql_user_role = "INSERT INTO sec_user_role(user_id, role_id) VALUES(?, ?)";
		executer.execute(sql_user_role, user_id, role.id);
	}
	out << '{"rs": 1}';
} catch(Exception e) {
	log.error(e.getMessage());
	out << '{"rs": 0}';
}
