<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Create Project</title>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
<script>
function create_project() {
	var json = JSON.stringify($("#create_project_form").serializeObject());
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: base_pre+'/project_action.groovy?op=create',
		data: json,
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
				case 1: $('#res_create_project').html('Successful!'); $("#create_project_form")[0].reset(); break;
				case 0: $('#res_create_project').html('Failed!'); break;
			}
		}
	});
	return false;
}
</script>
</head>
<body>
<c:import url="/tiles/title.jsp" charEncoding="UTF-8" />
<div class="center">
<section>
<h4>Create Project：</h4>
<div>
<form id="create_project_form">
<div>
Project Name：
</div>
<div>
<input id="project_name" name="project_name" type="text" style="width: 420px;" />
</div>
<div>
Project Text:
</div>
<div>
<textarea id="project_text" name="project_text" rows="20" style="width: 420px;"></textarea>
</div>
</form>
<a href="javascript:void(0);" onclick="create_project();">Create</a>
</div>
<div id="res_create_project">

</div>
</section>
</div>
<c:import url="/tiles/footer.jsp" charEncoding="UTF-8" />
</body>
</html>
