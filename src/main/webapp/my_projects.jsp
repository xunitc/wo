<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE HTML>
<html>
<head>
<title>My Projects</title>
<%
request.setAttribute("username", org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
%>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
<sql:setDataSource dataSource="jdbc/mysql" />
<sql:query var="users" sql="SELECT id FROM sec_user WHERE user_name = ?">
	<sql:param value="${username}" />
</sql:query>
<sql:query var="projects" sql="SELECT * FROM view_project WHERE user_id = ?">
	<sql:param value="${users.rows[0].id}" />
</sql:query>
</head>
<body>
<c:import url="/tiles/title.jsp" charEncoding="UTF-8" />
<div class="center">
<section>
<h4>Projects:</h4>
<table>
<tr>
<th style="width: 70em;">Project Name</th>
<th style="width: 10em;">Worker Name</th>
</tr>
<c:forEach items="${projects.rows}" var="project">
<tr>
	<td><a href='<c:url value="/project.jsp?id=${project.id}" />'>${project.project_name}</a></td>
	<td>${project.worker_name}</td>
</tr>
</c:forEach>
</table>
</div>
<c:import url="/tiles/footer.jsp" charEncoding="UTF-8" />
</body>
</html>
