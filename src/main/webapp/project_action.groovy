import groovy.sql.*;
import groovy.json.JsonSlurper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Hex;
import java.security.MessageDigest;
import org.apache.shiro.SecurityUtils;

final log = LoggerFactory.getLogger(this.getClass());

def p = request.reader.text;
def slurper = new JsonSlurper();
p = slurper.parseText(p);

response.setContentType("application/json; charset=utf-8");
try {
	def ctx = new javax.naming.InitialContext().lookup("java:/comp/env");
	def datasource = ctx.lookup("jdbc/mysql");
	def executer = Sql.newInstance(datasource);
	switch(params.op) {
		case 'create':
			/* 当前登录的用户 id */
			def user_id = SecurityUtils.getSubject().getSession().getAttribute("user_id");
			/* 创建任务 */
			String sql = "INSERT INTO project(user_id, project_name, project_text) VALUES(?, ?, ?)";
			executer.execute(sql, user_id, p.project_name, p.project_text);
			break;
		case 'create_post':
			/* 当前登录的用户 id */
			def user_id = SecurityUtils.getSubject().getSession().getAttribute("user_id");
			String sql = "INSERT INTO workorder(project_id, user_id, work_text) VALUES(?, ?, ?)";
			executer.executeInsert(sql, [p.project_id, user_id, p.work_text]);
			break;
		case 'solved':
			/* 当前登录的用户 id */
			def user_id = SecurityUtils.getSubject().getSession().getAttribute("user_id");
			String sql = "UPDATE project SET state = 1 WHERE id = ? AND user_id = ?";
			executer.executeUpdate(sql, [p.project_id, user_id]);
			break;
		case 'remove':
			//executer.execute(sql);
			break;
	}
	out << '{"rs": 1}';
} catch(Exception e) {
	log.error(e.getMessage());
	out << '{"rs": 0}';
}