<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE HTML>
<html>
<head>
<title>My Project</title>
<%
request.setAttribute("username", org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
%>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
<sql:setDataSource dataSource="jdbc/mysql" />

<!-- Get user id, if id = project.user_id or id = project.worker_id, can do next.-->
<sql:query var="users" sql="SELECT id FROM sec_user WHERE user_name = ?">
	<sql:param value="${username}" />
</sql:query>
<c:set var="user_id" value="${users.rows[0].id}" />

<sql:query var="projects" sql="SELECT * FROM view_project WHERE id = ?">
	<sql:param value="${param.id}" />
</sql:query>
<c:set var="project" value="${projects.rows[0]}" />
<!-- No authority -->
<shiro:lacksRole name="admin">  
	<c:if test="${not (user_id eq project.user_id || user_id eq project.worker_id)}">
		<c:redirect url="/login.jsp" />
	</c:if>
</shiro:lacksRole>

<sql:query var="workorders" sql="SELECT * FROM view_workorder WHERE project_id = ?">
	<sql:param value="${param.id}" />
</sql:query>
<script>
function create_workorder() {
	var json = JSON.stringify($("#workorder_form").serializeObject());
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: base_pre+'/project_action.groovy?op=create_post',
		data: json,
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
				case 1: $('#res_create_post').html('Successful!'); $("#workorder_form")[0].reset(); location.reload(); break;
				case 0: $('#res_create_post').html('Failed!'); break;
			}
		}
	});
	return false;
}
function solved(project_id) {
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: base_pre+'/project_action.groovy?op=solved',
		data: JSON.stringify({"project_id": project_id}),
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
				case 1: $('#res_create_post').html('Successful!'); $("#workorder_form")[0].reset(); location.reload(); break;
				case 0: $('#res_create_post').html('Failed!'); break;
			}
		}
	});
	return false;
}
</script>
<style>
.project_title {
	width: 100%;
	height: 2em;
	text-align: center;
}
.post_title {
	border-top: 1px solid blue;
}
.post_body {
	border-top: 1px solid blue;
	min-height: 50px;
}
</style>
</head>
<body>
<c:import url="/tiles/title.jsp" charEncoding="UTF-8" />
<div class="center">
<section>
<div class="project_title">
${project.project_name}
<c:if test="${user_id eq project.user_id}">
- <a href="javascript:void(0);" onclick="solved(${param.id});">Solved</a>
</c:if>
</div>
<div class="post_title">
${project.poster_name} - ${project.post_time}
</div>
<div class="post_body">
<pre>${project.project_text}</pre>
</div>
<c:forEach items="${workorders.rows}" var="workorder">
<div class="post_title">
${workorder.poster} - ${workorder.post_time}
</div>
<div class="post_body">
<pre>${workorder.work_text}</pre>
</div>
</c:forEach>
<c:if test="${project.state != 1}">
<div class="post_body">
<form id="workorder_form">
<input type="hidden" name="project_id" value="${project.id}" />
<textarea name="work_text" cols="120" rows="10"></textarea>
</form>
<a href="javascript:void(0);" onclick="create_workorder();">Create</a>
<div id="res_create_post"></div>
</div>
</c:if>
</div>
<c:import url="/tiles/footer.jsp" charEncoding="UTF-8" />
</body>
</html>
