<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<div class="nav">
<a href='<c:url value="/index.jsp" />'>Home</a>
<a href='<c:url value="/create_project.jsp" />'>Create Project</a>
<a href='<c:url value="/my_projects.jsp" />'>My Projects</a>
<a href='<c:url value="/regist.jsp" />'>Regist</a>
<a href='<c:url value="/login.jsp" />'>Login</a>
<a href='<c:url value="/logout.jsp" />'>Logout</a>
<%=org.apache.shiro.SecurityUtils.getSubject().getPrincipal() == null ? "" : org.apache.shiro.SecurityUtils.getSubject().getPrincipal()%>
</div>
<shiro:hasAnyRoles name="admin,engineer" >
<div class="nav">
<shiro:hasRole name="admin">
<a href='<c:url value="/admin/manage_user.jsp" />'>User Manage</a>
<a href='<c:url value="/admin/assign_project.jsp" />'>Assign Projects</a>
<a href='<c:url value="/admin/project_solved.jsp" />'>Solved Projects</a>
</shiro:hasRole>
<shiro:hasRole name="engineer">
<a href='<c:url value="/engineer/my_works_unsolved.jsp" />'>My Works[Unsolved]</a>
<a href='<c:url value="/engineer/my_works_solved.jsp" />'>My Works[Solved]</a>
</shiro:hasRole>
</div>
</shiro:hasAnyRoles>