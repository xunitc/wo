<!DOCTYPE HEML>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Regist</title>
<style>
form {
	width: 280px;
	border: 1px solid green;
	margin: 0 auto;
	padding: 2em;
}
</style>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
<script src="${basePath}/js/jquery-3.2.1.min.js"></script>
<script src="${basePath}/js/utils.js?v=1"></script>
<script>
function regist() {
	var json = JSON.stringify($("#regist_form").serializeObject());
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: base_pre+'/regist_action.groovy',
		data: json,
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
				case 1: $('#res_regist').html('Successful! <a href="${basePath}/login.jsp">Login</a>'); $("#regist_form")[0].reset(); break;
				case 0: $('#res_regist').html('Failed!'); break;
			}
		}
	});
	return false;
}
</script>
</head>
<body>
<form id="regist_form">
username:<input type="text" name="username"/><br />
password:<input type="password" name="password"/><br />
<a href="javascript:void(0);" onclick="regist();">Regist</a>
</div>
<div id="res_regist">

</div>
</form>
</body>
</html>