function fill_page(data) {
	var page = data.page;
	$('#page_size').html(page.page_size);
	$('#page_count').html(page.page_count);
	$('#page_count_under').html(page.page_count);
	$('#current_page_num').html(page.pn);
	$('#rows_count').html(data.rows_count);
	fill_data(data.rows);
	sessionStorage.setItem(cup.query_page, JSON.stringify(data.page));
}
function search() {
	var object = $("#"+cup.query_form).serializeObject();
	object.pn = 1;
	var json = JSON.stringify(object);
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: cup.query_url,
		data: json,
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
			case "1":
				sessionStorage.setItem(cup.query, JSON.stringify(object));
				sessionStorage.setItem("ext_attr", JSON.stringify(data.ext_attr));
				fill_page(data);
				break;
			case "0":
				$('#res').html('查询失败');
				break;
			}
		}
	});
	return false;
}
function goto_page(json) {
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: cup.query_url,
		data: json,
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
			case "1":
				fill_page(data);
				break;
			case "0":
				$('#res').html('查询失败');
				break;
			}
		}
	});
}

function page_first() {
	var query = JSON.parse(sessionStorage.getItem(cup.query));
	query.pn = 1;
	var json = JSON.stringify(query);
	goto_page(json);
	return false;
}
function page_last() {
	var query = JSON.parse(sessionStorage.getItem(cup.query));
	var page = JSON.parse(sessionStorage.getItem(cup.query_page));
	query.pn = page.page_count;
	var json = JSON.stringify(query);
	goto_page(json);
	return false;
}
function page_pre() {
	var query = JSON.parse(sessionStorage.getItem(cup.query));
	var page = JSON.parse(sessionStorage.getItem(cup.query_page));
	query.pn = page.pn-1 > 1 ? page.pn-1 : 1;
	var json = JSON.stringify(query);
	goto_page(json);
	return false;
}

function page_next() {
	var query = JSON.parse(sessionStorage.getItem(cup.query));
	var page = JSON.parse(sessionStorage.getItem(cup.query_page));
	query.pn = page.pn+1 < page.page_count ? page.pn+1 : page.page_count;
	var json = JSON.stringify(query);
	goto_page(json);
	return false;
}