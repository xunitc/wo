// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
Date.prototype.format = function (fmt) { //author: meizz 
	var o = {
		"M+": this.getMonth() + 1, //月份 
		"d+": this.getDate(), //日 
		"h+": this.getHours(), //小时 
		"m+": this.getMinutes(), //分 
		"s+": this.getSeconds(), //秒 
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
		"S": this.getMilliseconds() //毫秒 
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}

/* 某字符重复生成n次 */
String.prototype.times = function(n) { 
	return Array.prototype.join.call({length:n+1}, this); 
};

/* 表单序列化 */
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    var $radio = $('input[type=radio],input[type=checkbox]',this);
    $.each($radio,function(){
        if(!o.hasOwnProperty(this.name)){
            o[this.name] = '';
        }
    });
    return o;
};

/* 判断 name 属性为 checkbox_name 的复选框是否有被选中的 */
function is_checked(checkbox_name) {
	var checkboxes = $("input[name='"+checkbox_name+"']");
	for(let checkbox of checkboxes) {
		// 遍历所有复选框，只要有一个被选中，则返回 true
		if($(checkbox).prop("checked")){
			return true;
		}
	}
	// 已遍历结束，没有复选框被选中
	return false;
}

/* 全角转半角 */
function full2half(str) {
	var result = '';
	for(var i = 0; i < str.length; i++) {
		code = str.charCodeAt(i);//获取当前字符的unicode编码
		if (code >= 65281 && code <= 65373) {//在这个unicode编码范围中的是所有的英文字母已经各种字符
			result += String.fromCharCode(str.charCodeAt(i) - 65248);//把全角字符的unicode编码转换为对应半角字符的unicode码
		} else if(code == 12288) {//空格
			result += String.fromCharCode(str.charCodeAt(i) - 12288 + 32);
		} else {
			result += str.charAt(i);
		}
	}
	return result;
}

/* 半角转全角 */
function half2full(str){
	var result = "";
	for(var i = 0; i < str.length; i++) {
		if(str.charCodeAt(i) == 32) {
			result += String.fromCharCode(12288);
		} else if(str.charCodeAt(i) < 127) {
			result += String.fromCharCode(str.charCodeAt(i)+65248); 
		} else {
			result += str.charAt(i);
		}
	}
	return result; 
}

/* 检查身份证正确性 */
function check_idcard(id_card) {
	if(id_card.length != 18) {
		return false;
	}
	var sum = 0;
	var mast = [7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2];
	for(var i = 0; i < 17; i++) {
		sum += mast[i]*parseInt(id_card[i])
	}
	sum = sum % 11;
	var key = ['1','0','X','9','8','7','6','5','4','3','2'];
	if(key[sum] == id_card.charAt(17).toUpperCase()) {
		return true;
	} else {
		return false;
	}
}
base_pre = '/wo';