<%@page language="java" import="java.util.*,java.sql.*,javax.naming.*,javax.sql.DataSource" pageEncoding="utf-8"%>
<!DOCTYPE HEML>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Login</title>
<style>
form {
	width: 280px;
	border: 1px solid green;
	margin: 0 auto;
	padding: 2em;
}
</style>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
</head>
<%
String username = request.getParameter("username");
String password = request.getParameter("password");
if(username != null) {
	org.apache.shiro.subject.Subject subject = org.apache.shiro.SecurityUtils.getSubject();
	subject.login(new org.apache.shiro.authc.UsernamePasswordToken(username, password));
	if(org.apache.shiro.SecurityUtils.getSubject().getPrincipal() != null) {
		Context initContext = new InitialContext();
		Context envContext  = (Context)initContext.lookup("java:/comp/env");
		DataSource ds = (DataSource)envContext.lookup("jdbc/mysql");
		Connection conn = ds.getConnection();
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM sec_user WHERE user_name = ?");
		ps.setString(1, (String)org.apache.shiro.SecurityUtils.getSubject().getPrincipal());
		ResultSet rs = ps.executeQuery();
		rs.next();
		Long user_id = rs.getLong(1);
		conn.close();
		org.apache.shiro.SecurityUtils.getSubject().getSession().setAttribute("user_id", user_id);
		response.sendRedirect((String)request.getAttribute("basePath"));
	}
}
%>
<body>
<form action="" method="post">
username: <input type="text" name="username"/><br />
password: <input type="password" name="password"/><br />
<input type="submit" value="Login">
</form>
</body>
</html>