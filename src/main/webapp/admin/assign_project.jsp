<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Assign Projects</title>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
<sql:setDataSource dataSource="jdbc/mysql" />
<sql:query var="projects" sql="SELECT * FROM view_project WHERE state = -1" />
<sql:query var="users" sql="SELECT su.id, su.user_name, sur.role_id FROM sec_user AS su LEFT JOIN sec_user_role AS sur ON su.id = sur.user_id AND role_id = 2" />
<script>
function assign(project_id) {
	var user_id = $('#user_id').val();
	var json = JSON.stringify({'project_id': project_id, 'user_id': user_id});
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: base_pre+'/admin/project_action.groovy?op=assign',
		data: json,
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
				case 1: $('#res_assign').html('Successful!'); location.reload(); break;
				case 0: $('#res_assign').html('Failed!'); break;
			}
		}
	});
	return false;
}
</script>
</head>
<body>
<c:import url="/tiles/title.jsp" charEncoding="UTF-8" />
<div class="center">
<section>
<h4>Projects:</h4>
<div>
Assign to: 
<select id="user_id" name="user_id">
<c:forEach items="${users.rows}" var="user">
<option value="${user.id}">${user.user_name}</option>
</c:forEach>
</select>
</div>
<table>
<tr>
<th style="width: 70em;">Project Name</th>
<th style="width: 10em;">Worker Name</th>
<th style="width: 10em;">Assign</th>
</tr>
<c:forEach items="${projects.rows}" var="project">
<tr>
	<td><a href='<c:url value="/project.jsp?id=${project.id}" />'>${project.project_name}</a></td>
	<td>${project.worker_name}</td>
	<td><a href="javascript:void(0);" onclick="assign(${project.id});">Assign</a></td>
</tr>
</c:forEach>
</table>
<div id="res_assign"> </div>
</div>
<c:import url="/tiles/footer.jsp" charEncoding="UTF-8" />
</body>
</html>
