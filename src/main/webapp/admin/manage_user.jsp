<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE HTML>
<html>
<head>
<title>User Manage</title>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
<sql:setDataSource dataSource="jdbc/mysql" />
<sql:query var="roles" sql="SELECT id, role_name FROM sec_role">
</sql:query>
<script>
function create_user() {
	var json = JSON.stringify($("#create_user_form").serializeObject());
	$.ajax({
		type: "post",
		contentType: "application/json; charset=UTF-8",
		url: base_pre+'/admin/manage_user_action.groovy?op=create',
		data: json,
		dataType: 'json',
		success : function(data) {
			switch(data.rs) {
				case 1: $('#res_create_user').html('Successful!'); $("#create_user_form")[0].reset(); break;
				case 0: $('#res_create_user').html('Failed!'); break;
			}
		}
	});
	return false;
}
</script>
</head>
<body>
<c:import url="/tiles/title.jsp" charEncoding="UTF-8" />
<div class="center">
<section>
<div>Create User:</div>
<div>
<form id="create_user_form">
Username: <input name="username" type="text" />
Password: <input name="password" type="text" />
Role:
<select name="role_id">
<c:forEach items="${roles.rows}" var="role">
<option value="${role.id}">${role.role_name}</option>
</c:forEach>
</select>
</form>
<a href="javascript:void(0);" onclick="create_user();">Create</a>
</div>
<div id="res_create_user">

</div>
</section>
<section>

</section>
</div>
<c:import url="/tiles/footer.jsp" charEncoding="UTF-8" />
</body>
</html>
