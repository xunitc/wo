import groovy.sql.*;
import groovy.json.JsonSlurper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Hex;
import java.security.MessageDigest;

final log = LoggerFactory.getLogger(this.getClass());

def p = request.reader.text;
def slurper = new JsonSlurper();
p = slurper.parseText(p);

response.setContentType("application/json; charset=utf-8");
try {
	def ctx = new javax.naming.InitialContext().lookup("java:/comp/env");
	def datasource = ctx.lookup("jdbc/mysql");
	def executer = Sql.newInstance(datasource);
	switch(params.op) {
		case 'assign':
			/* 将任务指派给某个工程师, 并将 state 从 -1 更新为 0 */
			String sql = "UPDATE project SET worker_id = ?, state = 0 WHERE id = ?";
			executer.executeInsert(sql, [p.user_id, p.project_id]);
			break;
		case 'remove':
			break;
	}
	out << '{"rs": 1}';
} catch(Exception e) {
	log.error(e.getMessage());
	out << '{"rs": 0}';
}
