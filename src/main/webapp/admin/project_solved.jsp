<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Solved Projects</title>
<c:import url="/tiles/head.jsp" charEncoding="UTF-8" />
<sql:setDataSource dataSource="jdbc/mysql" />
<sql:query var="projects" sql="SELECT * FROM view_project WHERE state = 1" />
</head>
<body>
<c:import url="/tiles/title.jsp" charEncoding="UTF-8" />
<div class="center">
<section>
<h4>Solved Projects:</h4>
<table>
<tr>
<th style="width: 70em;">Project Name</th>
<th style="width: 10em;">Worker Name</th>
</tr>
<c:forEach items="${projects.rows}" var="project">
<tr>
	<td><a href='<c:url value="/project.jsp?id=${project.id}" />'>${project.project_name}</a></td>
	<td>${project.worker_name}</td>
</tr>
</c:forEach>
</table>
<div id="res_assign"> </div>
</div>
<c:import url="/tiles/footer.jsp" charEncoding="UTF-8" />
</body>
</html>
