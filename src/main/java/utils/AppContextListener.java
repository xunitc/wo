package utils;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.Properties;

@WebListener
public class AppContextListener implements ServletContextListener {
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		ServletContext ctx = servletContextEvent.getServletContext();
		try{
			Properties prop = new Properties();
			prop.load(ctx.getResourceAsStream("/WEB-INF/app.properties"));
			for (String key : prop.stringPropertyNames()) {
				ctx.setAttribute(key, prop.getProperty(key));
			}
		} catch(java.io.IOException ioe) {
			ioe.printStackTrace();
		}
	}
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		ServletContext ctx = servletContextEvent.getServletContext();
	}
}
